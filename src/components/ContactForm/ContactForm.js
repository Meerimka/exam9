import React, {Component} from 'react';
import './ContactForm.css';
import {Button, Card, CardImg, Col, Form, FormGroup, Input, Label} from 'reactstrap';


class ContactForm extends Component {
    constructor(props){
        super(props);
        if(props.contact){
            this.state = {...props.contact}
        }else{
            this.state= {
                name:'',
                phone: '',
                email: '',
                image: ''
            }
        }
    }

    valueChanged =(event) =>{
        const{name , value} = event.target;
        this.setState({[name]:value})
    };

    submitHandler = event=>{
        event.preventDefault();
       this.props.onSubmit({...this.state})
    };

    render() {
        return (
            <Form  onSubmit={this.submitHandler}>
                <FormGroup row>
                    <Label for="name" sm={2}>Name:</Label>
                    <Col sm={10}>
                        <Input type="text" name="name"  id="name"
                        value={this.state.name} onChange={this.valueChanged}
                         />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="email" sm={2}>Email:</Label>
                    <Col sm={10}>
                        <Input type="text" name="email"  id="email"
                               value={this.state.email} onChange={this.valueChanged}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="phone" sm={2}>Phone:</Label>
                    <Col sm={10}>
                        <Input type="text" name="phone"  id="phone"
                               value={this.state.phone} onChange={this.valueChanged}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="image" sm={2}>Photo:</Label>
                    <Col sm={10}>
                        <Input type="text" name="image"  id="image"
                               value={this.state.image} onChange={this.valueChanged}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="image" sm={2}>View a photo:</Label>
                    <Col sm={10}>
                        <Card>
                            <CardImg top width="100%" src={this.state.image.value} alt="Photo" />
                        </Card>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col sm={{size:10 , offset: 2}}>
                    <Button type="submit">Save</Button>
                    </Col>
                    <Col sm={{size:15 , offset: 4}}>
                        <Button >Return Back</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default ContactForm;