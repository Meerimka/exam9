import {FETCH_PROD_ERROR, FETCH_PROD_REQUEST, FETCH_PROD_SUCCESS} from "./actions";

const initialState ={
    contacts: {},
    loading: false,
    error: null,
};

const addContactsReducer = (state=initialState, action) =>{

    switch (action.type) {
        case FETCH_PROD_REQUEST: {
            return{
                ...state,
                loading: true,
            }
        }
        case FETCH_PROD_SUCCESS: {
            return{
                ...state,
                loading:false,
                contacts: action.contacts,
            }
        }
        case FETCH_PROD_ERROR: {
            return{
                ...state,
                loading:false,
                error: action.error
            }
        }
        default:
            return state;
    }
};

export default addContactsReducer;