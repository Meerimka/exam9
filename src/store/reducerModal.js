import {
    INFO, INFOCANCEL, SUCCONTACT,
} from "./actions";

const initialState={
    contact:{},
    information: false,
};

const reducerMenu = (state=initialState , action) => {
    switch (action.type) {
        case INFO:
            return{
                ...state,
                information: true,
                contact: action.contact
            };
        case INFOCANCEL:
            return{
                ...state,

                information: false,
            };
        case SUCCONTACT:
            return{
                ...state,
                information:false,
                contact:action.contact
            };
        default:
            return state;
    }


};

export default reducerMenu;