import axios from '../axios-contacts';

export const FETCH_PROD_REQUEST = 'FETCH_PROD_REQUEST';
export const FETCH_PROD_SUCCESS = 'FETCH_PROD_SUCCESS';
export const FETCH_PROD_ERROR = 'FETCH_PROD_ERROR';

export const  INFO = 'INFO';
export const INFOCANCEL ='INFOCANCEL';


export const orderReq = () =>({type: FETCH_PROD_REQUEST});
export const orderSuc = contacts => ({type: FETCH_PROD_SUCCESS,contacts});
export const orderFail = error =>({type: FETCH_PROD_ERROR});


export const info = (contact) =>{
    return dispatch =>{
        dispatch({type:INFO, contact});

    }
};

export const infoCancel = () =>{
    return dispatch=>{
        dispatch({type:INFOCANCEL})
    }
};


export const addContact = data =>{
    return dispatch =>{
        dispatch(orderReq());
        return axios.post('contacts.json', data) }};



export const takeContacts= () =>{
    return dispatch =>{
        dispatch(orderReq());
        axios.get('contacts.json').then(response =>{
                dispatch(orderSuc(response.data))
            },error => dispatch(orderFail(error))
        )
    }
};

export const getContact =() =>{
    return (dispatch,getState) =>{
        const contact =getState().contacts.contact;
        dispatch(orderReq());
        axios.get()
    }
}


export const deleteContact = contact => {
    return dispatch => {
        dispatch(orderReq());
        axios.delete('contacts/' + contact + '.json').then(
            response => {
                dispatch(takeContacts());
            },
            error => dispatch(orderFail(error))
        );
    }
};


export const editContact = contact => {
    return dispatch => {
        dispatch(orderReq());
        axios.put('contacts/' + contact + '.json').then(response =>{
                dispatch(orderSuc({...response.data , contact}))
            },error => dispatch(orderFail(error))
        );
    }
};


