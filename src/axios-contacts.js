import axios from 'axios';

const instance =axios.create({
    baseURL: 'https://contacts-junusova.firebaseio.com/'
});

export default instance;