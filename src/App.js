import React, {Component, Fragment} from 'react';
import {NavLink as RouterNavLink, Route, Switch} from 'react-router-dom';
import './App.css';
import {Collapse, Container, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from "reactstrap";
import NewContact from "./containers/NewContact/NewContact";
import Menu from "./containers/Contacts/Contacts";
import EditContact from "./containers/EditContact/EditContact";

class App extends Component {
    render() {
        return (
            <Fragment>
                <Navbar color="dark" light expand="md">
                    <NavbarBrand color="light">Contacts</NavbarBrand>
                    <NavbarToggler/>
                    <Collapse isOpen navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink tag={RouterNavLink} to="/contacts" color="light" exact>Contacts</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink tag={RouterNavLink} to="/add" color="light">New Contact</NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
                <Container>
                    <Switch>
                        <Route path="/" exact component={Menu}></Route>
                        <Route path="/contacts" component={Menu}></Route>
                        <Route path="/edit/:id" component={EditContact}></Route>
                        <Route path="/add" exact component={NewContact}/>
                        <Route render={() => <h1>Not Found !</h1>}/>
                    </Switch>
                </Container>
            </Fragment>
        );
    }
}

export default App;
