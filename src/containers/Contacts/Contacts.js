import React, {Component,Fragment} from 'react';
import {connect} from 'react-redux';
import './Contacts.css';
import {deleteContact, editContact, info, infoCancel, takeContacts} from "../../store/actions";
import Modal from "../../components/UI/Modal/Modal";
import ContactData from "../ContactData/ContactData";


class Contacts extends Component {

    componentDidMount() {
        this.props.takeContacts();
    };

    deleteContact = contact => {
        this.props.deleteContact(contact).then(() => {
            this.props.history.push('/contacts');
        })

    };

    editHandler = (id) => {
        this.props.editContact(id).then(() => {
            this.props.history.push(`/edit/${id}`);
        })
    };

    render() {
        return (
            <div>
                <Fragment>
                    <div className="ContactBox">
                        {this.props.contacts ?  Object.keys(this.props.contacts).map((contact, index) => {
                            return (
                                <div>
                                    <div key={index} className='Contacts' onClick={() => this.props.info(this.props.contacts[contact])}>
                                        <img className='PhotoImg' src={this.props.contacts[contact].image} alt=""/>
                                        <h3 className='ContactName'>{this.props.contacts[contact].name}</h3>
                                    </div>

                                    <Modal
                                        show={this.props.information}
                                        close={this.props.infoCancel}
                                    >
                                        <ContactData
                                            close={this.props.infoCancel}
                                            delete={()=>this.deleteContact(contact)}
                                            edit={()=>this.editHandler(contact)}
                                        />
                                    </Modal>
                                </div>
                            )
                        }) : null }

                    </div>
                </Fragment>
            </div>
        );
    }
}
const mapStateToProps =(state)=>({
    contacts: state.contacts.contacts,
    information: state.info.information

});
const mapDispatchToProps =(dispatch)=>({
    takeContacts: () => dispatch(takeContacts()),
    infoCancel: () =>dispatch(infoCancel()),
    info: (contact) => dispatch(info(contact)),
    deleteContact: contact => dispatch(deleteContact(contact)),
    editContact: contact =>dispatch(editContact(contact)),

});
export default connect(mapStateToProps, mapDispatchToProps) (Contacts);