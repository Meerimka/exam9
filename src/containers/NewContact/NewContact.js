import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {addContact} from "../../store/actions";
import ContactForm from "../../components/ContactForm/ContactForm";



class NewContact extends Component {

    addContact = data => {
        this.props.addContact(data).then(() => {
            this.props.history.push('/contacts');
        })

    };

    render() {
        return (
            <Fragment>
                <h1>Submit new dishes</h1>
                <ContactForm onSubmit={this.addContact}/>
            </Fragment>

        );
    }
}

const mapDispatchToProps =(dispatch)=>({
    addContact: data => dispatch(addContact(data)),
});

export default connect (null,mapDispatchToProps) (NewContact);