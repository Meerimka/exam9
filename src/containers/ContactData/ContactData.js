import React, {Component} from 'react';
import {connect} from 'react-redux';
import './ContactData.css';
import { deleteContact} from "../../store/actions";
import {Button, Card, CardBody, CardImg, CardSubtitle, CardText, CardTitle} from "reactstrap";





class ContactData extends Component {
    constructor(props){
        super(props);
    }



    render() {
        return(
            <div className="ContactData">
                <h4>Contact Data</h4>
                <Card>
                    <CardImg top width="70%" src={this.props.contact.image} alt="Photo" />
                    <CardBody>
                        <CardTitle>{this.props.contact.name}</CardTitle>
                        <CardSubtitle>{this.props.contact.email}</CardSubtitle>
                        <CardText>{this.props.contact.phone}</CardText>
                        <Button onClick={this.props.delete}>Delete</Button>
                        <Button onClick={this.props.edit}>Edit</Button>
                    </CardBody>
                </Card>
            </div>
        );
        }
}

const mapStateToProps = state=>{
    return{
    contact: state.info.contact,
    }
};
const mapDispatchToProps = dispatch =>{
    return{
        deletContact: () => dispatch(deleteContact()),
    }
};

export default connect(mapStateToProps,mapDispatchToProps) (ContactData);