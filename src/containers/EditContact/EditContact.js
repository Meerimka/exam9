import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import ContactForm from "../../components/ContactForm/ContactForm";
import {getContact} from "../../store/actions";

class EditContact extends Component {


    render() {
        let editbox = <ContactForm
             onSubmit={this.props.getContact}
             contact={this.props.contact}
        /> ;
        return (
           <Fragment>
               <h1>Edit Contact</h1>
               {editbox}
           </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    contact: null,
});

const mapDispatchToProps =(dispatch)=>({
    getContact: ()=>dispatch(getContact()),
});

export default connect(mapStateToProps, mapDispatchToProps)(EditContact);
